# from imageai.Detection import VideoObjectDetection
# import os
#
# execution_path = os.getcwd()
#
# detector = VideoObjectDetection()
# detector.setModelTypeAsYOLOv3()
# detector.setModelPath( os.path.join(execution_path , "yolo.h5"))
# detector.loadModel()
#
# video_path = detector.detectObjectsFromVideo(input_file_path=os.path.join(execution_path, "chris.mp4"),
#                             output_file_path=os.path.join(execution_path, "chris_1")
#                             , frames_per_second=40, log_progress=True)
# print(video_path)

# from imageai.Detection import VideoObjectDetection
# import os
# execution_path = os.getcwd()
# print('christopher',execution_path)
#
# def forSeconds(second_number, average_output_count):
#     print("SECOND : ", second_number)
#     print("Output average count for unique objects in the last second: ", average_output_count)
#     print("------------END OF A SECOND --------------")
#
#
# video_detector = VideoObjectDetection()
# video_detector.setModelTypeAsYOLOv3()
# video_detector.setModelPath(os.path.join(execution_path, "yolo.h5"))
# video_detector.loadModel()
#
#
# video_detector.detectObjectsFromVideo(input_file_path=os.path.join(execution_path, "chris1.mp4"),
#                                       output_file_path=os.path.join(execution_path, "chris_output1") ,
#                                       frames_per_second=20, per_second_function=forSeconds,  minimum_percentage_probability=30)

from imageai.Detection import VideoObjectDetection
import os
import cv2

execution_path = os.getcwd()

#
# camera = cv2.VideoCapture('192.168.1.100')
camera = cv2.VideoCapture("http://root:pass@192.168.1.100/axis-cgi/mjpg/video.cgi?fps=5")
# camera = cv2.VideoCapture("http://root:pass@192.168.1.100/mjpg/1/video.mjpg")

detector = VideoObjectDetection()
detector.setModelTypeAsRetinaNet()
detector.setModelPath(os.path.join(execution_path , "resnet50_coco_best_v2.0.1.h5"))
detector.loadModel()


video_path = detector.detectObjectsFromVideo(camera_input=camera,
                                output_file_path=os.path.join(execution_path, "camera_detected_video")
                                , frames_per_second=100, log_progress=True, minimum_percentage_probability=40)