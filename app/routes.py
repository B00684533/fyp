from flask import render_template, flash, redirect, url_for, render_template_string
from app import app
from app.forms import LoginForm
from flask_login import current_user, login_user
from app.models import User, NetworkDevice, PortScan, Incident, Script, InspectDevice
from flask_login import logout_user
from flask_login import login_required
from flask import request
from werkzeug.urls import url_parse
from app import db
from app.forms import RegistrationForm
import subprocess
import incidents
import scripts_page
import networkdevices
import portscan
import users
import os
import inspect
import urllib
import flask
import time
import datetime
from app import *
from werkzeug.utils import secure_filename
from random import choice


UPLOAD_FOLDER = '/Users/ChristopherDunne/PycharmProjects/fyp/app/object_detection'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif', 'mov'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


# UPLOAD_FOLDER_scripts = '/Users/ChristopherDunne/PycharmProjects/fyp/app/axis'
# app.config['UPLOAD_FOLDER_scripts'] = UPLOAD_FOLDER_scripts



'''
LOGIN, LOGOUT & REGISTER
'''


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index_home')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


'''
HOME PAGE
'''


@app.route('/index_home')
def index_home():
    number_incidents = Incident.query.count()
    number_scripts = Script.query.count()

    return render_template('home.html', number_incidents=number_incidents,
                           number_scripts=number_scripts)


'''
USERS
'''


@app.route('/users', methods=['GET', 'POST'])
def users_page():
    users.add_users()
    user_form = RegistrationForm()

    return render_template('users.html', user_form=user_form, user=users.show_users())


@app.route('/delete_user', methods=['GET', 'POST'])
def delete_users():
    return users.delete_user()


@app.route('/edit_user', methods=['GET', 'POST'])
def edit_users():
    return users.edit_user()


'''
INCIDENTS
'''
# https://www.codementor.io/garethdwyer/building-a-crud-application-with-flask-and-sqlalchemy-dm3wv7yu2
# ^ Get, Post, Update & Delete

@app.route('/incidents', methods=['GET'])
def incidents_index():
    error = None
    return render_template('incidents.html', incident=incidents.show_incidents(), error=error)


@app.route('/incidents', methods=['POST'])
def incidents_add():
    incidents.add_incidents()

    return redirect("/incidents")


@app.route("/update", methods=["POST"])
def update_incidents():
    return incidents.update_incident()


# Not actually specifying the id here, hence post possibly
@app.route("/delete", methods=["POST"])
@login_required
def delete_incidents():
    return incidents.delete_incident()


'''
SCRIPTS
'''


@app.route('/scripts', methods=['GET', 'POST'])
def scripts():
    scripts_page.add_scripts()
    return render_template("scripts.html", scripts=scripts_page.show_scripts())


@app.route("/update_scripts", methods=["POST"])
def update_scripts():
    return scripts_page.update_scripts()


@app.route("/execute_scripts", methods=["POST"])
def execute_scripts():
    script = request.form.get("script_executed")
    parameters = request.form.get("script_parameters")

    output = os.system('python app/axis/' + script + ' ' + parameters)
    print(output)

    if output == 0:
        flash('Successfully executed script ' + script, 'alert-success')
    else:
        flash('Failed to execute script ' + script, 'alert-danger')

    return redirect("/scripts")


@app.route("/delete_scripts", methods=["POST"])
def delete_scripts():
    return scripts_page.delete_scripts()


'''
NETWORK DEVICES
'''


@app.route('/device', methods=['GET', 'POST'])
def network_devices():
    return render_template("networkdevices.html")


@app.route("/delete_device", methods=["POST"])
def delete_device():
    name = request.form.get("ip_address")

    i = NetworkDevice.query.filter_by(ip_address=name).first()
    db.session.delete(i)

    db.session.commit()
    return redirect("/device")


'''
PORT SCANNING
'''


@app.route('/portscan', methods=['GET', 'POST'])
def port_scan():
    if request.form:
        try:
            ip_address = request.form.get("ipaddress_portscan")
            device_type = request.form.get("device")
            ports = subprocess.check_output(['python3', 'portscan.py', ip_address])

            p = PortScan(ip_address, ports, device_type)
            db.session.add(p)
            db.session.commit()

        except Exception as e:
            print("Failed to ")
            print(e)

    return render_template("portscan.html", portscan=portscan.show_port_info())


'''
IMAGE ANALYSIS
'''

#
# @app.route('/object_detection', methods=['GET', 'POST'])
# def object_detection():
#     ip_address = request.form.get("ip_address")
#     timestamp = datetime.datetime.now().strftime("%y-%m-%d-%H-%M")
#     if request.method == 'POST':
#         timestamp = datetime.datetime.now().strftime("%y-%m-%d-%H-%M")
#         urllib.urlretrieve("http://root:pass@"+ip_address+"/jpg/image.jpg",
#                            "/Users/ChristopherDunne/PycharmProjects/fyp/app/object_detection/"+timestamp+".jpg")
#
#         output = subprocess.check_output(['python3', 'object_detection.py',
#                                           '/Users/ChristopherDunne/PycharmProjects/fyp/app/object_detection/'+timestamp+'.jpg'])
#
#     new_image = '/Users/ChristopherDunne/PycharmProjects/fyp/app/object_detection/new_image.jpg'
#     return render_template("object_detection.html")


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/object_detection', methods=['GET', 'POST'])
def detect_objects():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))


            output = subprocess.check_output(['python3', 'object_detection.py',
                                              '/Users/ChristopherDunne/PycharmProjects/fyp/'
                                              'app/object_detection/'+filename])

            split_list = output.split('\n')
            split_list = split_list[:-1]

            return render_template("object_detection.html", split_list=split_list)

    return render_template("object_detection.html")


'''
DEVICE 
DETAILS
'''


@app.route('/count', methods=['GET'])
def count_devices():
    output = subprocess.check_output(['nmap', '-sP', '--max-rtt-timeout', '100ms', '192.168.1.0/24'])
    split_output = output.split()
    hosts = split_output[-7]
    devices = hosts.replace('(', '')
    return devices


@app.route('/rt_monitoring', methods=['GET'])
def real_time():
    return render_template_string('''
    <table class="table  table-striped">
     <thead>
            <tr>
                <th scope="col"> Device </th>
                <th scope="col"> Status </th>
            </tr>
     </thead>
    <tbody>

    {% for ip in list %}
            <tr>
                <td>{{ ip }}</td>
                <td>Connected</td>
            </tr>

    {% endfor %}
    </tbody>

    </table>
    ''', list=networkdevices.real_time_ping())


'''
Inspection
'''


@app.route('/inspect', methods=['GET', 'POST'])
def inspect_devices():
    if request.form:
        try:
            ip_address = request.form.get("ipaddress_inspect")
            output = subprocess.check_output(['nmap', ip_address])

            # Host is up or Host is down
            status = output.split('\n')[2]
            # print('chris,', line)
            if 'down' in status:
                print('device is down')
                # report device is down
            elif 'up' in status:
                print('device is up')

                # Device Type
                dev = output.split('for')[1].split('(', 1)[0]

                # Filtered and Unfiltered Ports
                ports = output.split('\n')[3]
                ports_line = ports.split(':', 1)[1]
                closed_ports = ports_line.split(',', 1)[0].split('closed', 1)[0]
                filtered_ports = ports_line.split(',', 1)[1].split('filtered', 1)[0]

                t = []
                # ALL the ports
                service_ports = output.split('\n')[5:-3]
                for x in service_ports:
                    number = x.split('open')[0].split('/')[0]
                    name = x.split('open')[1]
                    t.append(number + name)
                a = '\n'.join(t)
                print(a)

                p = InspectDevice(ip_address, dev, 'Active', closed_ports, filtered_ports, a)
                db.session.add(p)
                db.session.commit()

            else:
                pass
                print('problem')

        except Exception as e:
            print("Failed to ")
            print(e)

    return render_template("inspect.html", deviceinfo=inspect.show_device_info())
