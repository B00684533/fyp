from app import db
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import login


# User Table
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    email_1 = db.Column(db.String(120))
    permissions = db.Column(db.String(50))
    password_hash = db.Column(db.String(128))
    incidents = db.relationship('Incident', backref='author', lazy='dynamic')
    scripts = db.relationship('Script', backref='author', lazy='dynamic')
    networkdevices = db.relationship('NetworkDevice', backref='author', lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    # Use 3rd party package to hash password
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __init__(self, username, email, permissions):
        self.username = username
        self.email = email
        self.permissions = permissions


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


# Incident Table
class Incident(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    description = db.Column(db.String(255))
    description1 = db.Column(db.String(255))
    status = db.Column(db.String(255))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    # Initialising arguments every time Incident created.
    def __init__(self, name, description, status):
        self.name = name
        self.description = description
        self.status = status

    def __repr__(self):
        return '<Post {}>'.format(self.name)


# Script Table
class Script(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    description = db.Column(db.String(255))
    parameters = db.Column(db.String(10))
    documentation = db.Column(db.String(255))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    # Initialising arguments every time script created.
    def __init__(self, name, description, parameters, documentation):
        self.name = name
        self.description = description
        self.parameters = parameters
        self.documentation = documentation

    def __repr__(self):
        return '<Post {}>'.format(self.name)


# NetworkDevice Table
class NetworkDevice(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ip_address = db.Column(db.String(15))
    device_type = db.Column(db.String(255))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __init__(self, ip_address, device_type):
        self.ip_address = ip_address
        self.device_type = device_type

    def __repr__(self):
        return '<NetworkDevice {}>'.format(self.ip_address)


# PortScan Table
class PortScan(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ip_address = db.Column(db.String(15))
    open_ports = db.Column(db.String(255))
    device_type = db.Column(db.String(255))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __init__(self, ip_address, open_ports, device_type):
        self.ip_address = ip_address
        self.open_ports = open_ports
        self.device_type = device_type

    def __repr__(self):
        return '<OpenPorts {}>'.format(self.ip_address)


# Inspect Table
class InspectDevice(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ip_address = db.Column(db.Integer)
    device_type = db.Column(db.String(255))
    state = db.Column(db.String(10))
    closed_ports = db.Column(db.Integer)
    filtered_ports = db.Column(db.Integer)
    open_ports = db.Column(db.String(255))

    def __init__(self, ip_address, device_type, state, closed_ports, filtered_ports, open_ports):
        self.ip_address = ip_address
        self.device_type = device_type
        self.state = state
        self.closed_ports = closed_ports
        self.filtered_ports = filtered_ports
        self.open_ports = open_ports

    def __repr__(self):
        return '<Device Inspected {}>'.format(self.ip_address)

