from flask import request, redirect
from app.models import InspectDevice


def show_device_info():
    device_inspected = InspectDevice.query.all()
    return device_inspected
