#!/usr/bin/env python
import requests
import sys

ipaddress = sys.argv[1]
year = sys.argv[2]
month = sys.argv[3]
day = sys.argv[4]

result = requests.get('http://root:pass@'+ipaddress+'/axis-cgi/admin/date.cgi?'
                      'action=set&year='+year+'&month='+month+'&day='+day+'')

content = result.content
print(content)
