#!/usr/bin/env python
import requests
import datetime
import sys

ipaddress = sys.argv[1]

ip=ipaddress

# requires authentication
result = requests.get('http://root:pass@'+ip+'/axis-cgi/admin/systemlog.cgi')
content = result.content

timestamp = datetime.datetime.now().strftime("%d%m%y-%H_%M")

fh = open('/Users/ChristopherDunne/Desktop/axisM1013/system_logs_M1013'+timestamp+'.txt', 'w')

fh.write(content)

fh.close()
