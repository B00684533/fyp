$(document).ready(function() {

    $(".btn-color").on("click", function () {
        var $s_name = $(this).closest("tr")   // Finds the closest row <tr>
            .find(".script_name")     // Gets a descendent with class="script_name"
            .text();         // Retrieves the text within <td>

        var s_description = $(this).closest("tr")
            .find(".script_description")
            .text();

        var s_parameters = $(this).closest("tr")
            .find(".script_parameters")
            .text();

        var s_documentation = $(this).closest("tr")
            .find(".script_documentation")
            .text();

        $('#old_script_name').val($s_name);
        $('#new_script_name').val($s_name);
        $('#old_script_description').val(s_description);
        $('#new_script_description').val(s_description);
        $('#old_script_parameters').val(s_parameters);
        $('#new_script_parameters').val(s_parameters);
        $('#old_script_documentation').val(s_documentation);
        $('#new_script_documentation').val(s_documentation);
    });



    $(".btn-add-execute").on("click",function() {
        var $s_name = $(this).closest("tr")   // Finds the closest row <tr>
            .find(".script_name")     // Gets a descendent with class="script_name"
            .text();         // Retrieves the text within <td>

        var s_description = $(this).closest("tr")
            .find(".script_description")
            .text();

        var s_parameters = $(this).closest("tr")
            .find(".script_parameters")
            .text();

        var s_documentation = $(this).closest("tr")
            .find(".script_documentation")
            .text();

        $('#script_name_label').text($s_name);
        $('#script_executed').val($s_name);
        $('#script_description_label').text(s_description);
        $('#script_parameters_label').text(s_parameters);
        $('#script_documentation_label').text(s_documentation);
    });

    $('#add_script').click(function() {
        script_description = $('#add_script_description').val();
        script_parameters = $('#add_script_parameters').val();
        script_documentation = $('#add_script_documentation').val();

        if (script_description.length < 30) {
            alert('Please describe the script in more detail.');
            return false;
        }

        if (isNaN(script_parameters)) {
            alert('Only numeric values are allowed for parameters.');
            return false;
        }
    });


    $('#execute_script_button').click(function() {
        executing_script = $('#executing_script').val();
        if (executing_script.length === 0) {
            var strconfirm = confirm("Are you sure you want to execute this script with no parameters?");
            if (strconfirm == true) {
                return true;
            } else {
                return false;
            }
        }

    });

    $('#update_script').click(function() {
        new_script_description = $('#new_script_description').val();
        new_script_parameters = $('#new_script_parameters').val();

        if (new_script_description.length < 30) {
            alert('Please describe the script in more detail.');
            return false;
        }

        if (isNaN(new_script_parameters)) {
            alert('Only numeric values are allowed for parameters.');
            return false;
        }
    });

    setTimeout(function() {
        $('#user_message').fadeOut('fast');
    }, 8000); // <-- time in milliseconds

});
