$(document).ready(function() {

     $(".btn-color").on("click",function() {

         var $user_id = $(this).closest("tr")   // Finds the closest row <tr>
            .find(".user_id")     // Gets a descendent with class="user_id"
            .text();         // Retrieves the text within <td>

        var $s_name = $(this).closest("tr")
            .find(".user_name")
            .text();

        var $email = $(this).closest("tr")
            .find(".user_email")
            .text();

        var $permissions = $(this).closest("tr")
            .find(".user_permissions")
            .text();

        $('#user_id').val($user_id);

        $('#old_username').val($s_name);
        $('#new_username').val($s_name);
        $('#old_email').val($email);
        $('#new_email').val($email);
        $('#old_role').val($permissions);
        $('#new_role').val($permissions);
    });


    $('#submit').click(function() {
        username = $('#username').val();
        email = $('#email').val();
        password = $('#password').val();
        password2 = $('#password2').val();

        if (username.length < 6) {
            alert('Username must be more that 6 characters.');
            username.replace(username,"");
            email.replace(email,"");
            return false;
        }

        if (password != password2) {
            alert('Passwords do not match');
            return false;
        } else if (password2.length > 50) {
            alert('Password is too long');
            return false;
        } else if (password2.search(/[a-zA-Z]/) === -1) {
            alert("Password contains no letters.");
            return false;
        } else if (password2.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+]/) != -1) {
            alert("Password contains bad characters.");
            return false;
        } else if (password2.search(/\d/) === -1) {
            alert("Password contains no numbers.");
            return false;
        } else if (password2.length < 8) {
            alert("Password must contain more than 8 characters.");
            return false;
        }
    });

    setTimeout(function() {
        $('#user_message').fadeOut('fast');
    }, 8000); // <-- time in milliseconds

});