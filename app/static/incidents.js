$(document).ready(function() {

    // Update Incident
    $(".btn-color").on("click",function() {

        var $i_id = $(this).closest("tr")   // Finds the closest row <tr>
            .find(".incident_id")     // Gets a descendent with class="incident_id"
            .text();         // Retrieves the text within <td>

        var $i_name = $(this).closest("tr")
            .find(".incident_name")
            .text();

        var i_description = $(this).closest("tr")
            .find(".incident_description")
            .text();

        var i_status = $(this).closest("tr")
            .find(".incident_status")
            .text();


        $('#incident_id').val($i_id); // Hidden Element

        $('#newincidentname').val($i_name); // All "new" variables are being populated with the old data.
        $('#newincidentdescription').val(i_description);
        $('#newincidentstatus').val(i_status);

        // not necessary
        // $('#oldincidentname').val($i_name); // All "old" variables are hidden.
        // $('#oldincidentdescription').val(i_description);
        // $('#oldincidentstatus').val(i_status);

    });


    $('#show').click(function() {
        add_incident_name = $('#add_incident_name').val();
        add_incident_description = $('#add_incident_description').val();
        add_incident_status = $('#add_incident_status').val();

        if (add_incident_name.length === 0 ||
            add_incident_description.length === 0 ||
            add_incident_status.length === 0) {
            alert('Please fill in all fields.')
        }
    });

    $('#update_incident').click(function() {
        newincidentname = $('#newincidentname').val();
        newincidentdescription = $('#newincidentdescription').val();
        newincidentstatus = $('#newincidentstatus').val();

        if (newincidentname.length === 0 ||
            newincidentdescription.length === 0 ||
            newincidentstatus.length === 0) {
            alert('Please fill in all fields.')
        }
    });


    setTimeout(function() {
        // Remove this div after 8 seconds
        $('#user_message').fadeOut('fast');
    }, 8000);


 });