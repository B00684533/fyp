$(document).ready(function() {
    // Hide the timestamp as the function has not properly executed.
    $('#timestamp').hide();
    // When the page loads call "refreshTable" function every 40 seconds.
    setInterval("refreshTable()",40000);
});

function refreshTable() {
    // Make request to backend to get list of active hosts on the network
    // and load it into "ip_table" id.
    $("#iptable").load("/test", function() {
        $('#first').hide();

    });
    // Once the above action is completed, print the timestamp to show the
    // user when the script was last executed
    $("#timestamp").load("/timestamp", function() {});

}

function ajaxd_first() {
    $("#first").load("/test", function() {
        $('#loadingDiv').hide();
        $("#timestamp").load("/timestamp", function() {
            $('#timestamp').show();
        });

    });
}

// Once the above action is completed, print the timestamp to show the
// user when the script was last executed

// Make request to backend to get list of active hosts on the network
// and load it into "ip_table" id.

// Hide the timestamp as the function has not properly executed.

// When the page loads call "refreshTable" function every 40 seconds.