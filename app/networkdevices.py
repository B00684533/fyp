from flask import render_template, flash, redirect, url_for, render_template_string
from app import app
import subprocess
import datetime


def real_time_ping():
    output = subprocess.check_output(['python', 'new_ping.py'])
    print('christopher this is output: ', output)
    split_list = output.split('\n')
    final_list = split_list[:-1]
    return final_list


@app.route('/timestamp', methods=['GET'])
def show_timestamp():
    timestamp = datetime.datetime.now().strftime("%H:%M:%S")
    time = 'Last Updated: ' + timestamp
    return time
