#!/usr/bin/python2
import multiprocessing
import subprocess
import os
import csv


def pinger(job_q, results_q):

    DEVNULL = open(os.devnull, 'w')

    while True:
        ip = job_q.get()  # going through all ips in subnet mask
        if ip is None:
            break

        try:
            subprocess.check_call(['ping', '-c1', ip],
                                  stdout=DEVNULL, shell=False)
            results_q.put(ip)
            print()

        except:
            pass


if __name__ == '__main__':
    pool_size = 30

    jobs = multiprocessing.Queue()
    results = multiprocessing.Queue()

    pool = [multiprocessing.Process(target=pinger, args=(jobs, results))
            for i in range(pool_size)]

    # Start
    for p in pool:
        p.start()

    for i in range(1, 255):
        jobs.put('192.168.1.{0}'.format(i))

    # insert data into the queue
    for p in pool:
        # Put item into the queue. If optional args block is true and timeout is None (the default),
        # block if necessary until a free slot is available.
        jobs.put(None)

    # terminate
    for p in pool:
        p.join()


    while not results.empty():
        ip = results.get()
        status = 'active'
        row = [ip, status]
        print(ip,status)
