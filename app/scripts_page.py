from flask import request, redirect, flash
from app.models import Script, User
from app import db
from flask_login import current_user
from werkzeug.utils import secure_filename
from app import *
import os

UPLOAD_FOLDER_scripts = '/Users/ChristopherDunne/PycharmProjects/fyp/app/axis'
app.config['UPLOAD_FOLDER_scripts'] = UPLOAD_FOLDER_scripts



# Show Scripts
def show_scripts():
    scripts = Script.query.all()
    return scripts

# Add Scripts
def add_scripts():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        else:
            filename = secure_filename(file.filename)
            print(filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER_scripts'], filename))

        try:
            uploaded_file = request.form.get("script_name")
            description = request.form.get("description")
            parameters = request.form.get("parameters")
            documentation = request.form.get("documentation")

            current_username = current_user.username
            user = User.query.filter_by(username=current_username).first()

            if user.permissions == 'administrator':
                script = Script(uploaded_file, description, parameters, documentation)
                db.session.add(script)
                db.session.commit()
                flash('Successfully added script ' + uploaded_file, 'alert-success')
            else:
                flash('Due to user permission unable to add scripts.', 'alert-danger')

        except Exception as e:
            flash('Failed to add script ' + uploaded_file, 'alert-danger')
            print("Failed to add script")
            print(e)

# Update Scripts
def update_scripts():
    try:
        new_script_name = request.form.get("new_script_name")
        old_script_name = request.form.get("old_script_name")
        script_name = Script.query.filter_by(name=old_script_name).first()
        script_name.name = new_script_name

        new_script_description = request.form.get("new_script_description")
        old_script_description = request.form.get("old_script_description")
        script_description = Script.query.filter_by(description=old_script_description).first()
        script_description.description = new_script_description

        new_script_parameters = request.form.get("new_script_parameters")
        old_script_parameters = request.form.get("old_script_parameters")
        script_parameters = Script.query.filter_by(parameters=old_script_parameters).first()
        script_parameters.parameters = new_script_parameters

        new_script_documentation = request.form.get("new_script_documentation")
        old_script_documentation = request.form.get("old_script_documentation")
        script_documentation = Script.query.filter_by(documentation=old_script_documentation).first()
        script_documentation.documentation = new_script_documentation

        flash('Successfully updated script ' + old_script_name, 'alert-success')

        db.session.commit()

    except Exception as e:
        flash('Failed to update script ' + old_script_name, 'alert-danger')
        print("Couldn't update Script")
        print(e)

    return redirect("/scripts")


# Delete Scripts
def delete_scripts():
    name = request.form.get("script_name")

    current_username = current_user.username
    user = User.query.filter_by(username=current_username).first()

    if user.permissions == 'administrator':
        i = Script.query.filter_by(name=name).first()
        db.session.delete(i)
        db.session.commit()
        flash('Successfully deleted script ' + name, 'alert-success')
    else:
        flash('Due to user permission unable to add scripts.', 'alert-danger')

    return redirect("/scripts")
