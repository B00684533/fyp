from flask import request, redirect, flash
from app.models import User
from app import db
from app.forms import RegistrationForm
from flask_login import current_user


def show_users():
    users = User.query.all()
    return users

def add_users():
    user_form = RegistrationForm()

    if user_form.validate_on_submit():
        role = request.form.get("permissions")

        current_username = current_user.username
        user = User.query.filter_by(username=current_username).first()

        if user.permissions == 'administrator':
            user = User(username=user_form.username.data, email=user_form.email.data, permissions=role)
            user.set_password(user_form.password.data)
            db.session.add(user)
            db.session.commit()
            flash('Congratulations, you registered the user!', 'alert-success')
        else:
            flash('Due to user permission unable to register account.', 'alert-danger')
    else:
        print('Username or email already in use.')


def delete_user():
    current_username = current_user.username
    user = User.query.filter_by(username=current_username).first()

    if user.permissions == 'administrator':
        username = request.form.get("username")
        user = User.query.filter_by(username=username).first()
        db.session.delete(user)
        db.session.commit()
        flash('You have successfully deleted ' + username, 'alert-success')
    else:
        flash('Due to user permission unable to delete account.', 'alert-danger')

    return redirect("/users")


def edit_user():
    try:
        current_username = current_user.username
        user = User.query.filter_by(username=current_username).first()

        if user.permissions == 'administrator':
            user_id = request.form.get("user_id")

            u = User.query.get(user_id)

            # assigning new data from HTML to the database columns
            u.username = request.form.get("new_username")
            u.email = request.form.get("new_email")
            u.permissions = request.form.get("new_role")

            db.session.commit()

            # new_username = request.form.get("new_username")
            # old_username = request.form.get("old_username")
            # user_name = User.query.filter_by(username=old_username).first()
            # user_name.username = new_username
            #
            # new_email = request.form.get("new_email")
            # old_email = request.form.get("old_email")
            # user_email = User.query.filter_by(email=old_email).first()
            # user_email.email = new_email
            #
            # new_role = request.form.get("new_role")
            # old_role = request.form.get("old_role")
            # user_role = User.query.filter_by(permissions=old_role).first()
            # user_role.permissions = new_role
            #
            # db.session.commit()

            old_username = request.form.get("old_username")
            flash('You have successfully edited ' + old_username, 'alert-success')
        else:
            flash('Due to user permission unable to edit account.', 'alert-success')

    except Exception as e:
        flash("Unable to edit user", 'alert-danger')
        print(e)

    return redirect("/users")
