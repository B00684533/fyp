from flask import request, redirect, url_for
from app.models import Incident, User
from app import db
from flask_login import current_user
from flask import flash


def show_incidents():
    incidents = Incident.query.all()
    return incidents


def add_incidents():
    if request.form:
        try:
            name = request.form.get("name")
            description = request.form.get("description")
            status = request.form.get("status")
            i = Incident(name, description, status)
            db.session.add(i)
            db.session.commit()
            flash('Successfully added incident ' + name, 'alert-success')

        except Exception as e:
            print("Failed to add incident")
            print(e)
            flash('Failed to add incident ' + name, 'alert-danger')


def update_incident():
    try:
        incident_id = request.form.get("incident_id")

        i = Incident.query.get(incident_id)
        i.name = request.form.get("newincidentname")
        i.description = request.form.get("newincidentdescription")
        i.status = request.form.get("newincidentstatus")

        db.session.commit()
        flash('Successfully updated Incident Number ' + incident_id + '.', 'alert-success')

    except Exception as e:
        flash('Failed to update incident ' + incident_id + '.', 'alert-danger')
        print("Couldn't update Incident")
        print(e)

    return redirect("/incidents")


def delete_incident():
    name = request.form.get("name")

    current_username = current_user.username
    user = User.query.filter_by(username=current_username).first()

    if user.permissions == 'administrator':
        i = Incident.query.filter_by(name=name).first()
        db.session.delete(i)
        db.session.commit()
        flash('Successfully delete incident ' + name, 'alert-success')

    else:
        print('Due to user permission unable to delete incident.')
        flash('Failed to deleted incident ' + name, 'alert-danger')

    return redirect("/incidents")
