#!/usr/bin/env python
import requests
import sys

ipaddress = sys.argv[1]
name = sys.argv[2]
top = sys.argv[3]
bottom = sys.argv[4]
left = sys.argv[5]
right = sys.argv[6]

result = requests.get('http://root:pass@%s/axis-cgi/operator/param.cgi?action=add&'
                      'group=Motion&template=motion&Motion.M.Name=%s&Motion.M.Top=%s&'
                      'Motion.M.Bottom=%s&Motion.M.Left=%s&Motion.M.Right=%s' % (ipaddress, name, top,
                                                                                 bottom, left, right))
