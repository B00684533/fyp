"""PortScans table

Revision ID: a24ac03bcb39
Revises: 8de68933b224
Create Date: 2019-02-13 16:57:57.582539

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a24ac03bcb39'
down_revision = '8de68933b224'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('port_scan',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('ip_address', sa.String(length=15), nullable=True),
    sa.Column('open_ports', sa.String(length=255), nullable=True),
    sa.Column('device_type', sa.String(length=255), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_port_scan_timestamp'), 'port_scan', ['timestamp'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_port_scan_timestamp'), table_name='port_scan')
    op.drop_table('port_scan')
    # ### end Alembic commands ###
