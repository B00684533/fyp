"""users table

Revision ID: 8de68933b224
Revises: c41f86ba0179
Create Date: 2019-01-31 15:08:30.115546

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8de68933b224'
down_revision = 'c41f86ba0179'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user', sa.Column('permissions', sa.String(length=50), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('user', 'permissions')
    # ### end Alembic commands ###
