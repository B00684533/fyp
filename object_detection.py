from imageai.Detection import ObjectDetection
import os
from time import time
import sys
import datetime
from collections import Counter

# Also has dependancies like tensorflow opencv keras

in_image = sys.argv[1]

execution_path = os.getcwd()

detector = ObjectDetection()

# This function sets the model type of the object detection instance you created to the YOLOv3 model,
# which means you will be performing your object detection tasks using the pre-trained “YOLOv3” model
# you downloaded from the links above.
detector.setModelTypeAsYOLOv3()

# This function accepts a string which must be the path to the model file you downloaded and must
# corresponds to the model type you set for your object detection instance.
# Find example code,and parameters of the function below:
detector.setModelPath(os.path.join(execution_path , "yolo.h5"))

# This function loads the model from the path you specified in the function call above into your
# object detection instance.
detector.loadModel()

timestamp = datetime.datetime.now().strftime("%d%m%y-%H_%M")

# This is the function that performs object detection task after the model as loaded.
# It can be called many times to detect objects in any number of images.
detections = detector.detectObjectsFromImage(input_image=in_image,
                                             output_image_path=
                                             "/Users/ChristopherDunne/Desktop/image_detection/"+timestamp+".png",
                                             minimum_percentage_probability=30)

list = []
for eachObject in detections:
    print(eachObject["name"], eachObject["percentage_probability"])
    list.append(eachObject["name"])
print(Counter(list))
